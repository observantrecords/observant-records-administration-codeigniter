Observant Records Administration
================================

The Observant Records Administration site maintains discography information for Observant Records releases.

Features include:

* Export lyrics to text.
* Export track information with ISRC codes for audio file tagging.
* Generation of ISRC codes for Observant Records releases.

Release information maintained through this site is displayed on various Observant Records sites, include official sites for Eponymous 4 and Empty Ensemble.
