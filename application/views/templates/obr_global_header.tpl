					<header>
						<hgroup>
							{if (!empty($section_head))}<h2>{$section_head}</h2>{/if}
							
							{if (!empty($section_label))}<h3>{$section_label}</h3>{/if}
							
							{if (!empty($section_sublabel))}<h4>{$section_sublabel}</h4>{/if}
						</hgroup>
					</header>
